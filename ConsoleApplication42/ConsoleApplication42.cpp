﻿#include <iostream>
#include <time.h>
#include <conio.h>
using namespace std;

void main()
{
	setlocale(LC_CTYPE, "Rus");
	int a[100];
	int i, n, summa_otr = 0, kmin = 0, kmax = 0, proizv = 1, proizv1 = 1;
	cout << "Введите кол-во элементов массива : ";
	cin >> n;
	for (i = 0; i < n; i++)
	{
		cout << "A[" << i << "] = ";
		cin >> a[i];
	}
	cout << "\n";
	for (i = 0; i < n; i++)
	{
		if (a[i] < 0)
		{
			summa_otr = summa_otr + a[i];
		}
	}
	cout << "\n";
	cout << "Cумма отрицательных элементов = " << summa_otr << "\n";
	int min = a[0];
	for (i = 0; i < n; i++)
	{
		if (a[i] < min)
		{
			min = a[i];
			kmin = i;
		}
	}
	cout << "Минимальное значение массива : " << min << "\n";
	cout << "Минимальный элемент массива :" << kmin << "\n";
	int max = 0;
	for (i = 0; i < n; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
			kmax = i;
		}
	}
	cout << "Максимальное значение массива : " << max << "\n";
	cout << "Максимальный элемент массива : " << kmax << "\n";
	if (kmax > kmin)
	{
		for (i = kmin; i <= kmax; i++)
		{
			proizv = proizv * a[i];
		}
	}
	else
	{
		for (i = kmax; i <= kmin; i++)
		{
			proizv = proizv * a[i];
		}
	}
	cout << "Произведение между минимальным и максимальным элементами массива = " << proizv << "\n";
	for (i = 0; i < n; i++)
	{
		if (i % 2 == 0)
		{
			proizv1 = proizv1 * a[i];
		}
	}
	cout << "Произведение элементов с нечетными номерами : " << proizv1 << "\n";
	int one_chislo = 0;
	int kone_chislo = 0;
	for (i = 0; i < n; i++)
	{
		if (a[i] < 0)
		{
			one_chislo = a[i];
			kone_chislo = i; break;
		}
	}
	int two_chislo = 0;
	int ktwo_chislo = 0;
	for (i = n - 1; i >= 0; i--)
	{
		if (a[n - 1] < 0)
		{
			two_chislo = a[n - 1];
			ktwo_chislo = n - 1; break;
		}
	}
	int summotr = 0;
	if (kone_chislo > ktwo_chislo)
	{
		for (i = ktwo_chislo; i < n; i++)
		{
			summotr = summotr + a[i];
		}
	}
	else
	{
		for (i = kone_chislo; i < n; i++)
		{
			summotr = summotr + a[i];
		}
	}
	cout << "Сумма элементов между первым и последним отрицательным числом : " << summotr << "\n";
	_getch();
}